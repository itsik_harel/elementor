#!/Users/itsikh/dev/Elementor/venv/bin/python3.8
import psycopg2
from configparser import ConfigParser
import json
import pandas as pd
from io import StringIO
import base64
import requests
import platform
import pickle
import os
import datetime
import time

## params
source_csv_url = 'https://elementor-pub.s3.eu-central-1.amazonaws.com/Data-Enginner/Challenge1/request1.csv'
pg_target_table = 'elementor'
site_url = 'https://www.virustotal.com/api/v3/urls/'
site_key = '2c23b24e711aadb56866e505fdec4ab265f3ca700c25ffb59ca77c83a4dfdecb'
stale_threshold = 60  # in minutes

def config(filename='database.ini', section='postgresql'):
    # create a parser
    parser = ConfigParser()
    # read config file
    parser.read(filename)

    # get section, default to postgresql
    db = {}
    if parser.has_section(section):
        params = parser.items(section)
        for param in params:
            db[param[0]] = param[1]
    else:
        raise Exception('Section {0} not found in the {1} file'.format(section, filename))

    return db


def unload_to_pg(df, table):
    """ Connect to the PostgreSQL database server
    :df the origin pandas dataframe
    :table target PostgreSQL table
    """

    conn = None
    try:
        # read connection parameters
        params = config()

        # connect to the PostgreSQL server
        print('Connecting to the PostgreSQL database...')
        conn = psycopg2.connect(**params)

        # create a cursor
        copy_from_stringio(conn, df, table)

        # close PostgreSQL server server connection
        conn.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()
            print('Database connection closed.')

def copy_from_stringio(conn, df, table):
    # save dataframe to an in memory buffer
    buffer = StringIO()
    df.to_csv(buffer, index_label='id', header=False)
    buffer.seek(0)

    cursor = conn.cursor()
    try:
        cursor.copy_from(buffer, table, sep=",")
        conn.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        print("Error: %s" % error)
        conn.rollback()
        cursor.close()
        return 1
    print("copy_from_stringio() done")
    cursor.close()


def creation_date(path_to_file):
    """
    Try to get the date that a file was created, falling back to when it was
    last modified if that isn't possible.
    See http://stackoverflow.com/a/39501288/1709587 for explanation.
    """
    if platform.system() == 'Windows':
        return os.path.getctime(path_to_file)
    else:
        stat = os.stat(path_to_file)
        try:
            return stat.st_birthtime
        except AttributeError:
            # We're probably on Linux. No easy way to get creation dates here,
            # so we'll settle for when its content was last modified.
            return stat.st_mtime

def refresh_from_url(pkl_file):
    url_id = base64.urlsafe_b64encode(target_site.encode()).decode().strip("=")
    url = site_url + url_id
    headers = {'x-apikey': site_key}
    r = requests.get(url, headers=headers)
    # print(r.text)
    d = json.loads(r.text)
    pickle.dump(d, open(pkl_file, "wb"))
    return d

if __name__ == '__main__':

    try:

        sites_list = pd.read_csv(source_csv_url, header=None)
        print(sites_list.head())
        sites_list.rename(columns={sites_list.columns[0]: "site"}, inplace=True)

        main_result_dict = {}
        for index, row in sites_list.iterrows():
            print(row['site'])
            target_site = (row['site'])
            url_id = base64.urlsafe_b64encode(target_site.encode()).decode().strip("=")
            pkl_file = 'current_url_id_' + url_id + '.pkl'

            ## Check if local DB is stale
            if os.path.exists(pkl_file):

                threshold = datetime.timedelta(minutes=stale_threshold)  # can also be minutes, seconds, etc.
                filetime = os.path.getmtime(pkl_file)  # filename is the path to the local file you are refreshing
                now = time.time()
                delta = datetime.timedelta(seconds=now - filetime)
                if delta > threshold:
                    print('Local copy is Stale - refreshing')
                    d = refresh_from_url(pkl_file)
                else:
                    print('Using local file')
                    d = pickle.load(open(pkl_file, "rb"))
                    print(d)
            else:
                print('No local copy found, refreshing')
                d = refresh_from_url(pkl_file)

            last_analysis_stats_results = (d['data']['attributes']['last_analysis_stats'])
            # print(last_analysis_stats_results)
            status = 'safe'
            result_dict = {}
            for key, value in last_analysis_stats_results.items():
                if key in ('malicious', 'suspicious') and value > 0:
                    status = 'risk'
                    break
                else:
                    status = 'safe'
            total_votes_results = (d['data']['attributes']['total_votes'])
            result_dict['last_analysis_stats_results'] = last_analysis_stats_results
            result_dict['total_votes_results'] = total_votes_results
            result_dict['test_status'] = status
            categories = (d['data']['attributes']['categories']['Forcepoint ThreatSeeker'])
            result_dict['categories'] = categories
            # print(result_dict)
            main_result_dict[target_site] = result_dict

        result_df = pd.DataFrame.from_dict(main_result_dict)

        unload_to_pg(result_df, pg_target_table)
        print(result_df.head())
        print(result_df.columns)

    except Exception as e:
        print('error: ' + str(e))




